<?php

namespace App\Http\Controllers;

use App\User;
use App\CreditUser;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        dd($request->spend_credit);



        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Respons
     */
    public function show($id)
    {
        $user = User::find($id);

        $actual_date = Carbon::now();
        $expired_plan = $user->plan->final_date;

        $final_date = Carbon::parse($expired_plan);


        if($actual_date->gt($final_date)){
            $user->credit_user->update([
                'user_id'=> $user->id,
                'total_credits' => 0
            ]);
        }

        return view('credit.show', [
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('credit.edit', [
            'user' => User::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

        $user = User::find($id);

        if(request('spend_credit') > $user->credit_user->total_credits){
            // echo "El monto a descontar no puede ser mayor al monto disponible <br>";
            // echo "<a href=''><button>Return</button></a>";

            return back()->with('error', 'The credit entered is greater than the available credit');



        }else{
            $total_credits = $user->credit_user->total_credits - request('spend_credit');
            //dd($total_credits);

            $user->credit_user->update([
                'user_id' => $user->id,
                'total_credits' => $total_credits
            ]);
            return redirect()->route('users.index');
        }


        // $total_credits = $user->credit_user->total_credits - request('spend_credit');
        // //dd($total_credits);

        // $user->credit_user->update([
        //     'user_id' => $user->id,
        //     'total_credits' => $total_credits
        // ]);


        //return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
