<?php

namespace App\Http\Controllers;

use App\CreditUser;
use App\HistoryPlan;
use App\User;
use App\Plan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\SaveUserRequest;
use Illuminate\Support\Facades\Hash;


class userController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $users = User::get();

        $users = User::paginate(5);

        $actual_date = Carbon::now();

        //$users = User::with('plan')->get();
        return view('users.index', [
            'users' => $users,
            'actual_date' => $actual_date
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $plans = Plan::get();
        return view('users.create', compact('plans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveUserRequest $request)
    {

        // User::create($request->all());

        $user= User::create(array_merge($request->only('name', 'plan_id', 'email'),[
            'password' => Hash::make($request->password)
        ]));
        //dd($user->id);
        //$ultimo_id = User::latest('created_at')->first();

        HistoryPlan::create([
            'user_id' => $user->id,
            'plan_id' => request('plan_id')
        ]);

        CreditUser::create([
            'user_id' => $user->id,
            'total_credits' => $user->plan->credit
        ]);


        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::FindOrfail($id);

        $actual_date = Carbon::now();
        $expired_plan = $user->plan->final_date;

        $final_date = Carbon::parse($expired_plan);

        if($actual_date->gt($final_date)){
            $user->credit_user->update([
                'user_id'=> $user->id,
                'total_credits' => 0
            ]);
        }



        return view('users.show', [
            'user' => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //dd($user);
        $plans = Plan::get();

        return view('users.edit', [
             'user' => $user,
             'plans' => $plans
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user)
    {
        $old_credit = $user->credit_user->total_credits;

        $user->update([
            'name' => request('name'),
            'email' => request('email'),
            'plan_id' => request('plan_id'),
            'password' => Hash::make(request('password'))
        ]);

        HistoryPlan::create([
            'user_id' => $user->id,
            'plan_id' => request('plan_id')
        ]);

        $new_credit = $old_credit + $user->plan->credit;


        $user->credit_user->update([
            'user_id'=> $user->id,
            'total_credits' => $new_credit
        ]);



        return redirect()->route('users.show', $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('users.index');
    }



}
