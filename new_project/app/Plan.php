<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{

    protected $guarded = [];

    protected $casts = [
        'final_date' => 'datetime:Y-m-d',
    ];

    //protected $table = 'plans';

    public function users(){
        $this->hasMany(User::class);
    }
    public function history_users(){
        return $this->belongsToMany(User::class, 'history_plans')->withTimestamps();
    }
}
