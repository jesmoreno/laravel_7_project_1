<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Plan;
use Faker\Generator as Faker;

$factory->define(Plan::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'cost' => $faker->numberBetween($min = 1000, $max = 10000)
    ];
});
