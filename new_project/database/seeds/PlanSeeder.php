<?php

use App\Plan;
use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Plan::class, 3)->create();

        // $plan = new Plan();

        // $plan->name = 'Basic';
        // $plan->description = 'Lo que sea';
        // $plan->cost = 1000;

        // $plan->save();


    }
}
