<?php

use App\Plan;
use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(User::class, 5)->create();

        // $user_plan = Plan::where('name', 'Basic')->first();
        // $plan_id = $user_plan->id;

        // $user = new User();
        // $user->plan_id = $plan_id;
        // $user->name = 'Adrian';
        // $user->email = 'Adrian@gmail.com';
        // $user->name = 'Adrian';
        // $user->password = bcrypt('query');
        // $user->save();
        //$user->plan()->attach($plan_user);

    }
}
