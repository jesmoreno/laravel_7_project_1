@extends('layout')

@section('content')
    <div class="container">




        <form method="POST" action=" {{ route('credit.update', $user) }} "  class="bg-white py-3 px-4 shadow rounded">
            @csrf @method('PATCH')


            @if (session('error'))
                <div class="alert alert-primary añert-dismissible fade show" role="alert">

                    {{ session('error')}}

                    <button type="button"
                        class="close"
                        data-dismiss="alert"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
            @endif

            <h1> {{ $user->name }} </h1>

            @if ($errors->any())
                <div class="alert alert-primary">
                    <ul class="m-0">
                        @foreach ($errors->all() as $error)
                            <li>
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <fieldset>
                <div class="form-group">
                    <label>
                        Disponible Credits -->
                    </label>
                    {{ '  ' . $user->credit_user->total_credits . '$' }}
                    {{-- <?php $rest_credit = $user->credit_user->total_credits?> --}}

                </div>
                <div class="form-group">
                    <label>
                        Credits to consume <br>
                    </label>
                        <input class="form-control border-0 bg-light shadow-sm"
                            type="numeric" name="spend_credit">

                </div>
                <input type="Submit" value="Save" name="Save" id="Save">
            </fieldset>
        </form>
    </div>

@endsection
