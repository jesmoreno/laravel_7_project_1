<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>@yield('title')</title>

    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
    <div id="app" class="d-flex h-screen flex-column justify-content-between" >
        <header>
            @include('partial.nav')
        </header>
        <hr>
        <main class="py-4">
            @yield('content')
        </main>
        <hr>
        <footer class=" bg-white text-center text-black-50 py-3 shadow">
            {{config('app.name') . ' | Copyright @ ' . date('Y') }}
        </footer>
    </div>
</body>
</html>
