<nav class="fixed-top navbar navbar-expand-lg bg-white">
    <div class="container">
        <a class="navbar-brand" href=" {{ route('home')}} ">
             {{config('app.name')}}
        </a>
        <ul class="nav nav-pills">

            <li class="nav-item">
                <a class="nav-link" href=" {{route('users.index')}} ">Users</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href=" {{route('plans.index')}} ">Plans</a>
            </li>
        </ul>
    </div>
</nav>
