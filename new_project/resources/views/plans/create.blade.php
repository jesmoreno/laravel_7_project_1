@extends('layout')

@section('content')
    <div class="container">

        <div class="row">

            <div class="col-12 col-sm-10 col-lg-8 mx-auto">



                <form class="bg-white py-3 px-4 shadow rounded"
                    method="POST" action=" {{ route('plans.store') }} ">

                    @csrf




                    <h1 class="display-4 ">
                        New plan
                    </h1>

                    @if($errors->any())
                        <div class="alert alert-primary">
                            <ul class="m-0">
                                @foreach ($errors->all() as $error)
                                    <li>
                                        {{$error}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">
                        <label >
                            Name of the plan <br>
                        </label>
                            <input class="form-control border-0 bg-light shadow-sm"
                            type="text" name="name">

                    </div>

                    <div class="form-group">
                        <label >
                            description of the plan <br>
                        </label>
                            <input class="form-control border-0 bg-light shadow-sm"
                            type="text" name="description">

                    </div>

                    <div class="form-group">
                        <label >
                            Cost of the plan <br>
                        </label>
                            <input class="form-control border-0 bg-light shadow-sm"
                            type="numeric" name="cost">
                    </div>
                    <div class="form-group">
                        <label >
                            Credit of the plan <br>
                        </label>
                            <input class="form-control border-0 bg-light shadow-sm"
                            type="numeric" name="credit">
                    </div>
                    <div class="form-group">
                        <label >
                            Final date of the plan <br>
                        </label>
                            <input class="form-control border-0 bg-light shadow-sm"
                            type="date" name="final_date">
                    </div>

                    <button class="btn btn-primary btn-lg btn-block">
                        Save
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
