@extends('layout')

@section('content')
    <div class="container">
        <div class="col-12 col-sm-10 col-lg-8 mx-auto">


            <form class="bg-white py-3 px-4 shadow rounded"
                method="POST" action=" {{ route('plans.update', $plan) }} ">
                @csrf @method('PATCH')

                <h1 class="display-4">Edit plan</h1>
                <div class="form-group">
                    <label >
                        Name of the plan
                    </label>
                        <input class="form-control border-0 bg-light shadow-sm"
                            type="text" name="name" value=" {{ $plan->name }} ">

                </div>

                <div class="form-group">
                    <label >
                        description of the plan
                    </label>
                        <input class="form-control border-0 bg-light shadow-sm"
                            type="text" name="description" value=" {{ $plan->description }} ">

                </div>

                <div class="form-group">
                    <label >
                        Cost of the plan
                    </label>
                        <input  class="form-control border-0 bg-light shadow-sm"
                            type="text" name="cost" value=" {{ $plan->cost }} ">

                </div>

                <div class="form-group">
                    <label >
                        Modify final date {{$plan->final_date}}
                    </label>
                        <input  class="form-control border-0 bg-light shadow-sm"
                            type="date" name="final_date" value=" {{ $plan->final_date }} ">

                </div>

                <button class="btn btn-primary btn-lg btn-block">
                    Update
                </button>
            </div>
        </div>
    </form>
@endsection
