@extends('layout')

@section('content')

    <div class="container">

        <span class="float-right">{{ $actual_date->format('d/m/Y') }}</span>
        <br>


        <div class="d-flex justify-content-between align-items-center">
            <h1 class="display-4 mb-0">All Plans</h1>

            <a
                href=" {{ route('plans.create')}} ">
                <button class="btn btn-primary">
                    Create a new plan
                </button>
            </a>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table table-primary table-striped table-bordered border-primary table-sm text-center">
                <thead>
                <tr>
                    <th scope="col">Plan_id</th>
                    <th scope="col">Title</th>
                    <th scope="col">Cost</th>
                    <th scope="col">Credit</th>
                    <th scope="col">Final date of plan</th>
                    <th scope="col">Creation Date</th>
                    <th scope="col">See details</th>
                </tr>
                </thead>
                <tbody >
                    @foreach ($plans as $plan)
                        <tr >
                            <th scope="row"> {{ $plan->id}} </th>
                            <td> {{ $plan->name}} </td>
                            <td> {{ $plan->cost . ' $ ' }} </td>
                            <td> {{ $plan->credit . ' $ ' }} </td>
                            <td> {{ $plan->final_date->format('d/m/Y')}} </td>
                            <td> {{ $plan->created_at->format('d/m/Y')}} </td>
                            <td><a class="" href="{{ route('plans.show', $plan)}} ">
                                <button class=" btn btn-outline-primary text-dark">
                                    See plan
                                </button>
                            </a></td>
                        </tr>

                    @endforeach

                </tbody>
            </table>
        </div>

    </div>

@endsection

