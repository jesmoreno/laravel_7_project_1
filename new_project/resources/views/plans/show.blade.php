@extends('layout')

@section('content')

    <div class="container">
        <div class="bg-white p-5 shadow rounded">


                <h1 class="display-4"> {{ $plan->name }} </h1>

                <p class="text-secondary">
                    {{ $plan->description }}
                </p>
                <p class="text-secondary-50">
                    {{ $plan->cost. ' $ ' }}
                </p>
                <p class="text-secondary-50">
                    {{ $plan->credit . ' $ '  }}
                </p>
                <p class="text-secondary-50">
                    {{ $plan->created_at  }}
                </p>
                <p class="text-secondary-50">
                    {{ $plan->final_date  }}
                </p>

                <div class="d-flex justify-content-between">
                    <a
                        href=" {{ route('plans.edit', $plan)}} ">
                        <button class="btn btn-primary">
                            Edit
                        </button>
                    </a>
                    <br>
                    <form method="POST" action=" {{route('plans.destroy', $plan)}} ">
                        @csrf
                        @method('DELETE')

                        <button class="btn btn-danger">
                            Remove
                        </button>
                    </form>
                </div>

        </div>
    </div>
@endsection
