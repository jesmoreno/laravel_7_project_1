@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-10 col-lg-8 mx-auto">

                <form class="bg-white py-3 px-4 shadow rounded"
                    method="POST" action=" {{route('users.store')}} ">

                    @csrf

                    <h1 class="display-4 ">New user</h1>

                    @if ($errors->any())
                        <div class="alert alert-primary">
                            <ul class="m-0">
                                @foreach ($errors->all() as $error)
                                    <li>
                                        {{ $error }}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">
                        <label>
                            UserName <br>
                        </label>
                            <input class="form-control border-0 bg-light shadow-sm"
                                type="text" name="name">
                    </div>

                    <div class="form-group">
                        <label>
                            Email <br>
                        </label>
                            <input class="form-control border-0 bg-light shadow-sm"
                                type="text" name="email">
                    </div>

                    <div class="form-group">
                        <label>
                            Password <br>
                        </label>
                            <input class="form-control border-0 bg-light shadow-sm"
                                type="password" name="password">
                    </div>

                    <div class="form-group">
                        <select name="plan_id" id="" class="form-control">
                            <option value=""> Choose Plan</option>
                            @foreach ($plans as $plan)

                                <option value=" {{ $plan->id }} "> {{$plan->name}} </option>

                            @endforeach
                        </select>
                    </div>

                    <button class="btn btn-primary btn-lg btn-block">
                        Save
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
