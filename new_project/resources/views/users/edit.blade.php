@extends('layout')

@section('content')
    <div class="container">
        <div class="col-12 col-sm-10 col-lg-8 mx-auto">

            <form class="bg-white py-3 px-4 shadow rounded"
                method="POST" action=" {{route('users.update', $user)}} ">
                @csrf @method('PATCH')

                <h1 class="display-4"> User info</h1>

                <div class="form-group">
                <label>
                    UserName
                </label>
                    <input class="form-control border-0 bg-light shadow-sm"
                        type="text" name="name" value="{{ $user->name }}">
                </div>

                <div class="form-group">
                <label>
                    Email
                </label>
                    <input class="form-control border-0 bg-light shadow-sm"
                        type="text" name="email" value=" {{ $user->email}} ">
                </div>

                <div class="form-group">
                <label>
                    Password
                </label>
                    <input  class="form-control border-0 bg-light shadow-sm"
                        type="password" name="password" value=" {{ $user->password}} ">
                </div>

                <div class="form-group">
                        <select name="plan_id" class="form-control">
                            <option> {{$user->plan->name}} </option>
                            @foreach ($plans as $plan)
                                <option value=" {{ $plan->id }} "> {{ $plan->name }} </option>
                            @endforeach
                        </select>
                </div>

                <button class="btn btn-primary btn-lg btn-block">
                    Update
                </button>
            </form>
        </div>
    </div>

@endsection
