@extends('layout')

@section('content')

    <div class="container">

        <span class="float-right">{{ $actual_date->format('d/m/Y') }}</span>
        <br>

        <div class="d-flex justify-content-between align-items-center">
            <h1 class="display-4 mb-0">
                All Users
            </h1>

            <a
                href=" {{ route('users.create') }} ">
                <button class="btn btn-primary">
                    Create a new user
                </button>
            </a>
        </div>

        <hr>

        <div class="table-responsive">
            <table class="table table-primary table-striped table-bordered border-primary table-sm text-center">
                <thead>
                <tr>
                    {{-- <th scope="col">User_id</th> --}}
                    <th scope="col">Name</th>
                    <th scope="col">Associate Plan</th>
                    <th scope="col">Disponible Credits</th>
                    <th scope="col">Final date of plan</th>
                    <th scope="col">See details</th>
                    <th scope="col">Credits</th>
                    <th scope="col">Freeze</th>
                </tr>
                </thead>
                <tbody >
                    @foreach ($users as $user)
                        <tr >
                            {{-- <th scope="row"> {{ $user->id}} </th> --}}
                            <td> {{ $user->name}} </td>
                            <td> {{ $user->plan->name }} </td>
                            <td> {{ $user->credit_user->total_credits . '$' }} </td>
                            <td> {{ $user->plan->final_date->format('d/m/Y')}} </td>
                            <td>
                                @if ($user->freeze_defrost == 0)
                                <a  href="{{ route('users.show', $user)}} ">
                                    <button class=" btn btn-outline-primary text-dark">
                                        See user
                                    </button>
                                </a>
                                @else
                                    <button class=" btn btn-outline-primary text-dark">
                                        Blocked
                                    </button>
                                @endif

                            </td>
                            <td>
                                @if ($user->freeze_defrost == 0)
                                <a  href=" {{ route('credit.show', $user) }} ">
                                    <button class=" btn btn-outline-primary text-dark">
                                        Credit user
                                    </button>
                                </a>
                                @else
                                    <button class=" btn btn-outline-primary text-dark">
                                        Blocked
                                    </button>
                                @endif

                            </td>
                            <td>
                                @if ($user->freeze_defrost == 0)
                                    <a  href=" {{ route('freeze.show', $user) }} ">
                                        <button class=" btn btn-outline-primary text-dark">
                                            freeze plan
                                        </button>
                                    </a>
                                @endif
                                @if ($user->freeze_defrost == 1)
                                    <a  href=" {{ route('freeze.show', $user) }} ">
                                        <button class=" btn btn-outline-primary text-dark">
                                            defrost plan
                                        </button>
                                    </a>
                                @endif

                            </td>
                        </tr>

                    @endforeach

                </tbody>
            </table>
        </div>
        {{$users->links()}}
    </div>




@endsection

