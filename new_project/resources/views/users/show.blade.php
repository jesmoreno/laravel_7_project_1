@extends('layout')

@section('content')
    <div class="container">
        <div class="bg-white p-5 shadow rounded">
            <div class=" justify-content-between p-2 col-6 float-left">
                <h3 class="display-8">
                    {{ 'Name: ' . $user->name }}
                </h3>
                <p class="text-secondary">
                    {{ 'Email: ' . $user->email}}
                </p>

                <p class="text-secondary-50">
                    {{ 'Plan: ' . $user->plan->name}}
                </p>

                <p class="text-secondary-50">
                    {{ 'Disponible credits: ' . $user->credit_user->total_credits . '$'}}
                </p>

                <p class="text-secondary-50">
                    {{ 'Password: ' . $user->password}}
                </p>
            </div>

                {{-- @foreach ($user->history_plans as $history)
                    {{ $history->name }}
                @endforeach --}}
                <div class="table-responsive col-6 float-right">
                    <table class="table table-primary table-striped table-bordered table-hover table-sm text-center">
                        <div class=" justify-content-between">
                            <thead>
                            <tr>
                                {{-- <th scope="col">User_id</th> --}}
                                <th scope="col">History Plans</th>
                                <th scope="col">Date</th>

                            </tr>
                            </thead>
                            <tbody >
                                @foreach ($user->history_plans as $history)
                                    <tr >
                                        {{-- <th scope="row"> {{ $user->id}} </th> --}}
                                        <td> {{ $history->name}} </td>
                                        <td> {{ $history->created_at->format('d/m/Y')}} </td>

                                    </tr>

                                @endforeach

                            </tbody>
                        </div>
                    </table>

               </div>
            <br>


            <div class="d-flex justify-content-between col-12">
                <a
                    href=" {{ route('users.edit', $user)}} ">
                    <button class="btn btn-primary">
                        Edit
                    </button></a>

                <form method="POST" action=" {{route('users.destroy', $user)}} ">
                    @csrf
                    @method('DELETE')

                    <button class="btn btn-danger">
                        Delete
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
